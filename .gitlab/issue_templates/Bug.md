# Bug Issue

<!-- PLEASE ADD APPROPRIATE PRIORITY, TYPE, AND CATEGORY LABELS -->

## Description

<!--What problem did occur and how what may be the issue? -->

## Order to Approach

<!-- How one can reproduce the issue - this is very important. Include Command Line / other instructions that show the bug! -->

<!-- How to best attempt the issue and possible fixes -->

<!-- If you can, link to the line of code that might be responsible for the problem -->

### Bug-Checklist

- [ ] Can you reproduce the problem?
- [ ] Are you running the latest version?
- [ ] Are you in the master/development branch?
- [ ] Have you installed the newest requirements?

### Steps to Reproduce the Problem

<!-- Add relevant logs and screenshots -->

1.

### Specifications

 <!-- Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise. -->

- Pythonversion:

## Actual Behavior

<!--Describe what the actual behavior is? -->

## Expected Behavior

<!--Describe what the expected behavior is? -->

## Estimated Weight / Value

<!-- How much time may be needed to fulfill the task before asking for help, and importance to the project as a whole. -->

## Acceptance Criteria for Merge

<!-- What is needed for a merge request-->

/label ~bug
