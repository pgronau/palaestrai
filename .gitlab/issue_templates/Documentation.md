# Documentation Issue

<!-- PLEASE ADD APPROPRIATE PRIORITY, TYPE, AND CATEGORY LABELS -->

## Description

Issue Description: The documentation for <!-- ... --> is not sufficient.

## What should be documented?

<!-- Does a function or a component have to be documented? If so, please name the component and what is currently not sufficiently documented. If a tutorial is needed, please describe what you intend to do. -->

## Where have you looked for that information?

<!--Where have you tried to find the information? -->

## Order to Approach

<!-- How to best attempt the issue -->

## Estimated Weight / Value

<!-- How much time may be needed to fulfill the task before asking for help, and importance to the project as a whole. -->

## Acceptance Criteria for Merge

<!-- What is needed for a merge request-->

/label ~Documentation
