# palaestrai-minimal -- minimal base image for palaestrAI
#
# The base image contains the extended Python basis along with the packages
# palaestrAI needs in any case. Derived versions either check out palaestrAI
# or install it from PyPI, depending on the "BUILD_TYPE" argument.
#
# BUILD_TYPE:
#   BUILD_TYPE=development  Check out current development from git
#   BUILD_TYPE=production   Install palaestrai from PyPI
#
# PYTHONVERSION:
#   Set the version of the Python base image
#
# GIT_BRANCH:
#   Give a branch to switch to after a git clone. Only valid for the
#   "development" image. Defaults to "development"

ARG PYTHONVERSION=3.9
ARG BUILD_TYPE=development

FROM python:$PYTHONVERSION AS base
ENV TZ=UTC
ENV DEBIAN_FRONTEND noninteractive
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo "$TZ" > /etc/timezone
RUN apt-get clean \
    && apt-get update \
    && apt-get install -y \
        wget \
        sudo \
        curl \
        sqlite3 \
        postgresql-client \
        python3-pip \
    && python3 -m pip install -U pip \
    && python3 -m pip install -UI llvmlite

RUN useradd -Um -G users palaestrai
RUN mkdir /palaestrai \
    && mkdir -p /workspace \
    && chown palaestrai:palaestrai /workspace \
    && chmod 1770 /workspace


FROM base AS development
WORKDIR /palaestrai
COPY ["generate-requirements.py", \
      "setup.py", \
      "README.rst", \
      "VERSION", \
      "tox.ini", \
      "mypy.ini", \
      "containers/start.sh", \
      "./"]
COPY ["src",  "src"]
COPY ["doc",  "doc"]
COPY ["tests", "tests"]
COPY ["palaestrai_completion.sh", \
      "palaestrai_completion.zsh", \
      "palaestrai_completion.fish", \
      "./"]
RUN apt-get install -y \
        build-essential \
        pandoc \
        graphviz \
        libgraphviz-dev
RUN python3 ./generate-requirements.py \
    && python3 -m pip install -r requirements.txt \
    && python3 -m pip install -e '.[dev]'
RUN chmod 0755 /palaestrai/start.sh \
    && chown -R palaestrai:users /palaestrai
ENTRYPOINT ["/palaestrai/start.sh"]


FROM development AS testing
ENTRYPOINT []


FROM base AS production
RUN python -m pip install -U palaestrai
WORKDIR /workspace
ENTRYPOINT ["/palaestrai/start.sh"]


FROM $BUILD_TYPE AS final
RUN apt-get autoremove -y \
    && apt-get autoclean \
    && rm -rf /var/lib/{apt,dpkg,cache,log}