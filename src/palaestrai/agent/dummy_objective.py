from __future__ import annotations

from typing import TYPE_CHECKING, List

from palaestrai.agent.objective import Objective

if TYPE_CHECKING:
    from palaestrai.agent import RewardInformation


class DummyObjective(Objective):
    def __init__(self, params):
        super().__init__(params)

    def internal_reward(self, rewards: List[RewardInformation], **kwargs):
        final_rewards = []
        for reward in rewards:
            r = reward.reward_value
            final_rewards.append(r)
        return sum(final_rewards)
