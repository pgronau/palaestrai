from ctypes import Union

import numpy as np
import pandas as pd
from typing import List, Tuple

from palaestrai.agent import (
    RewardInformation,
    ActuatorInformation,
    SensorInformation,
)


def _init_actions(actions: List[ActuatorInformation]):
    """Initialize the actions"""
    action_ids = [a.actuator_id for a in actions]
    return pd.DataFrame(columns=action_ids, dtype=np.float32)


def _init_observations(observations: List[SensorInformation]):
    """Initialize the observations"""
    observation_ids = [o.sensor_id for o in observations]
    return pd.DataFrame(columns=observation_ids, dtype=np.float32)


def _init_internal_rewards():
    return pd.DataFrame(columns=["internal_reward"], dtype=np.float32)


def _init_rewards(env_rewards: List[RewardInformation]):
    """Initialize the env rewards"""
    reward_ids = [r.reward_id for r in env_rewards]
    return pd.DataFrame(columns=reward_ids, dtype=np.float32)


class Memory:
    """A memory for the brain class to store the data

    The memory is given to the objective to calculate
    the internal reward. Add all information needed to
    this object. This class should be overwritten
    if needed.
    Attributes:
        rewards (pd.DataFrame): The env rewards of the agent
        actions (pd.DataFrame): The actions of the agent
        observations (pd.DataFrame): The observations of the agent
        internal_reward (pd.DataFrame): The rewards of the agent
    """

    def __init__(
        self,
        rewards: List[RewardInformation],
        actions: List[ActuatorInformation],
        observations: List[SensorInformation],
    ):
        self._rewards = _init_rewards(rewards)
        self._actions = _init_actions(actions)
        self._observations = _init_observations(observations)
        self._internal_reward = _init_internal_rewards()

    @property
    def rewards(self):
        return self._rewards

    @rewards.setter
    def rewards(self, value):
        self._rewards = value

    @property
    def actions(self):
        return self._actions

    @actions.setter
    def actions(self, value):
        self._actions = value

    @property
    def observations(self):
        return self._observations

    @observations.setter
    def observations(self, value):
        self._observations = value

    @property
    def internal_reward(self):
        return self._internal_reward

    @internal_reward.setter
    def internal_reward(self, value):
        self._internal_reward = value

    def append_rewards(self, rewards: List[RewardInformation]):
        try:
            dictionary = {
                reward.reward_id: reward.reward_value for reward in rewards
            }
            self.rewards = pd.concat(
                [self.rewards, pd.DataFrame(dictionary, index=[0])],
                ignore_index=True,
            )
        except Exception as e:
            raise e

    def append_actions(self, actions: List):
        try:
            dictionary = {
                action.actuator_id: action.setpoint for action in actions
            }
            self.actions = pd.concat(
                [self.actions, pd.DataFrame(dictionary, index=[0])],
                ignore_index=True,
            )
        except Exception as e:
            raise e

    def append_observations(self, observations: List):
        try:
            dictionary = {
                observation.sensor_id: observation.sensor_value
                for observation in observations
            }
            self.observations = pd.concat(
                [self.observations, pd.DataFrame(dictionary, index=[0])],
                ignore_index=True,
            )
        except Exception as e:
            raise e

    def append_internal_rewards(self, reward: float):
        try:
            r = {"internal_reward": reward}
            self.internal_reward = pd.concat(
                [self.internal_reward, pd.DataFrame(r, index=[0])],
                ignore_index=True,
            )
        except Exception as e:
            raise e

    def append(
        self,
        env_rewards: List[RewardInformation],
        actions: List[ActuatorInformation],
        observations: List[SensorInformation],
        rewards: float,
    ):
        """Add all data to the memory"""
        self.append_rewards(env_rewards)
        self.append_actions(actions)
        self.append_observations(observations)
        self.append_internal_rewards(rewards)

    def get_last_row(
        self,
    ) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
        """Get the last row of the memory

        Returns:
            DataFrame: The last row of the memory
        """
        return self.get_last_rows(1)

    def get_last_rows(
        self, n: int
    ) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
        """Get the last n rows of the memory

        Returns:

            Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]: The last n rows of the memory
        """
        return (
            self.rewards.iloc[-n:],
            self.actions.iloc[-n:],
            self.observations.iloc[-n:],
            self.internal_reward.iloc[-n:],
        )

    def get_last_reward(self):
        """Get the last env reward of the memory"""
        return self.rewards.iloc[-1]

    def get_last_action(self):
        """Get the last action of the memory"""
        return self.actions.iloc[-1]

    def get_last_observation(self):
        """Get the last observation of the memory"""
        return self.observations.iloc[-1]

    def get_last_internal_reward(self):
        """Get the last reward of the memory"""
        return self.internal_reward.iloc[-1]

    def get_full_memory(self):
        """Get the full memory"""
        return (
            self.rewards,
            self.actions,
            self.observations,
            self.internal_reward,
        )

    def get_rewards(self):
        """Get the env rewards of the memory"""
        return self.rewards

    def get_actions(self):
        """Get the actions of the memory"""
        return self.actions

    def get_observations(self):
        """Get the observations of the memory"""
        return self.observations

    def get_internal_rewards(self):
        """Get the rewards of the memory"""
        return self.internal_reward
