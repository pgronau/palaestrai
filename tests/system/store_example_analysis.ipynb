{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9b8e27d0",
   "metadata": {},
   "source": [
    "# Analysis Example with the palaestrAI Store\n",
    "\n",
    "This notebook is both a tutorial and a system test. It runs an example experiment, which contains two run phases as well as two agents. After running the experiment, we will get all data out of the store and plot some nice graphs. Since the agents we use for brains perform only random actions, there won't be much to focus on content-wise. However, the tutorial shall serve as a pointer on how to use the palaestrAI core infrastructure (and not hARL, etc.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c5fe5e0",
   "metadata": {},
   "source": [
    "## Imports\n",
    "\n",
    "Let's start by importing necessary modules. This will be what we need for palaestrAI, namely the entrypoint, the runtime config, and the database access stuff:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b182db2e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import palaestrai  # Will provide palaestrai.exectue\n",
    "import palaestrai.core  # RuntimeConfig\n",
    "import palaestrai.store  # store.Session for database connectivity\n",
    "import palaestrai.store.database_util\n",
    "import palaestrai.store.database_model as paldb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7972b02",
   "metadata": {},
   "source": [
    "The typical data science analysis toolstack uses *pandas* and *matplotlib*, so let's import those, too. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "09cf68b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a5bf7b6",
   "metadata": {},
   "source": [
    "*jsonpickle* we will need to inspect the reward information objects later on. Here, we also need to use the jsonpickle extension for numpy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51d1087c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import jsonpickle\n",
    "import jsonpickle.ext.numpy as jsonpickle_numpy\n",
    "\n",
    "jsonpickle_numpy.register_handlers()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a6843e7",
   "metadata": {},
   "source": [
    "There are also some of the usual suspects from Python's standard library, which we'll import here without further comment:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9894c198",
   "metadata": {},
   "outputs": [],
   "source": [
    "import io\n",
    "import os\n",
    "import pprint\n",
    "import tempfile\n",
    "from pathlib import Path"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6095bd1b",
   "metadata": {},
   "source": [
    "## Experiment Run Document\n",
    "\n",
    "Everything palaestrAI does depends on its configuration, or rather, *experiments*. When you do real design of experiments, you first create an *experiment* document, in which you define strategies for sampling your factors. Each sample is an *experiment run*, which will be executed by palaestrAI. We won't do the full DoE dance here, but rather provide an experiment run document directly.\n",
    "\n",
    "Experiments and experiment runs have **unique names** (`uid`). When they're not given, they are auto-generated, but usually the user wants to set them in order to find them in the store later on. Choosing a good name might seem hard (it isn't, any string will do); being force to choose a *unique* names might seem an unecessary constraint. However, it isn't: Each experiment run must be repeatable, i.e., always have the same result, no matter how often it is run. A change in an experiment run definition can yield different results. Therefore, each experiment run is unique—and thus should be its name, too. We will define the experiment run name as a separate variable so that we don't have to remember it later on when we query the store:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "535b0ff2",
   "metadata": {},
   "outputs": [],
   "source": [
    "experiment_run_name = \"Tutorial Experiment Run\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "159543fc",
   "metadata": {},
   "source": [
    "Experiment (run) documents also have a **version**. It serves as a discriminator to catch semantic changes in the document. It is an additional safeguard and emits a log message, but not a stopgap. \n",
    "\n",
    "For this tutorial, we set the document's version to palaestrAI's version. That is okay here since we need to keep this documented up-to-date in any case. When experiment runs are archived, the version number (and its immutability!) become more important."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbdc8cae",
   "metadata": {},
   "outputs": [],
   "source": [
    "experiment_run_version = \"3.4.1\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ea94d25",
   "metadata": {},
   "source": [
    "And now to the document itself. Apart from the `uid`, the `version`, and the random seed (`seed`), it provides the configuration of the experiment run. Experiment runs have *phases*, so the most important key here is the experiment `schedule`.\n",
    "\n",
    "a **schedule** defines the phases of an experiment run. A phase is comprised of environments, agents, simulation paramaters such as the termination condition, as well as general configuration flags. Schedule configurations are cascading: Values defined in the previous phase are applied to following phases, too, unless they are explicitly overwritten.\n",
    "\n",
    "In our example, we have three phases in our schedule. The first phase trains only one agent, the second trains two in the same environment, and finally, there is a third phase as testing stage.\n",
    "\n",
    "(*Please note* that we're using an f-string here, and hence the YAML dict `{}` becomes `{{}}`.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7af1b51e",
   "metadata": {},
   "outputs": [],
   "source": [
    "experiment_run_document = f\"\"\"\n",
    "uid: \"{experiment_run_name}\"\n",
    "seed: 47  # Not quite Star Trek, but...\n",
    "version: \"{experiment_run_version}\"\n",
    "schedule:  # The schedule for this run; it is a list\n",
    "  - phase_0:\n",
    "      environments:  # Definition of the environments for this phase\n",
    "        - environment:\n",
    "            name: palaestrai.environment.dummy_environment:DummyEnvironment\n",
    "            uid: denv\n",
    "            params: {{ }}\n",
    "      agents:  # Definiton of agents for this phase\n",
    "        - name: mighty_defender\n",
    "          brain:\n",
    "            name: palaestrai.agent.dummy_brain:DummyBrain\n",
    "            params: {{ }}\n",
    "          muscle:\n",
    "            name: palaestrai.agent.dummy_muscle:DummyMuscle\n",
    "            params: {{ }}\n",
    "          objective:\n",
    "            name: palaestrai.agent.dummy_objective:DummyObjective\n",
    "            params: {{\"params\": 1}}\n",
    "          sensors: [denv.0, denv.1, denv.2, denv.3, denv.4]\n",
    "          actuators: [denv.0, denv.1, denv.2, denv.3, denv.4]\n",
    "      simulation:  # Definition of the simulation controller for this phase\n",
    "        name: palaestrai.simulation:VanillaSimController\n",
    "        conditions:\n",
    "        - name: palaestrai.simulation:VanillaSimControllerTerminationCondition\n",
    "          params: {{ }}\n",
    "      phase_config:  # Additional config for this phase\n",
    "        mode: train\n",
    "        worker: 1\n",
    "        episodes: 5 \n",
    "  - phase_1:  # Name of the current phase. Can be any user-chosen name\n",
    "      agents:  # Definiton of agents for this phase\n",
    "        - name: mighty_defender\n",
    "          brain:\n",
    "            name: palaestrai.agent.dummy_brain:DummyBrain\n",
    "            params: {{ }}\n",
    "          muscle:\n",
    "            name: palaestrai.agent.dummy_muscle:DummyMuscle\n",
    "            params: {{ }}\n",
    "          objective:\n",
    "            name: palaestrai.agent.dummy_objective:DummyObjective\n",
    "            params: {{\"params\": 1}}\n",
    "          sensors: [denv.0, denv.1, denv.2, denv.3, denv.4]\n",
    "          actuators: [denv.0, denv.1, denv.2, denv.3, denv.4]\n",
    "        - name: evil_attacker\n",
    "          brain:\n",
    "            name: palaestrai.agent.dummy_brain:DummyBrain\n",
    "            params: {{ }}\n",
    "          muscle:\n",
    "            name: palaestrai.agent.dummy_muscle:DummyMuscle\n",
    "            params: {{ }}\n",
    "          objective:\n",
    "            name: palaestrai.agent.dummy_objective:DummyObjective\n",
    "            params: {{\"params\": 1}}\n",
    "          sensors: [denv.5, denv.6, denv.7, denv.8, denv.9]\n",
    "          actuators: [denv.5, denv.6, denv.7, denv.8, denv.9]\n",
    "      simulation:  # Definition of the simulation controller for this phase\n",
    "        name: palaestrai.simulation:VanillaSimController\n",
    "        conditions:\n",
    "        - name: palaestrai.simulation:VanillaSimControllerTerminationCondition\n",
    "          params: {{ }}\n",
    "      phase_config:  # Additional config for this phase\n",
    "        mode: train\n",
    "        worker: 1\n",
    "        episodes: 2\n",
    "  - phase_2:  # Definition of the second phase. Keeps every information\n",
    "              # from the first except for those keys that are redefined\n",
    "              # here.\n",
    "      phase_config:  \n",
    "        mode: test\n",
    "        episodes: 3\n",
    "run_config:  # Not a runTIME config\n",
    "  condition:       \n",
    "    name: palaestrai.experiment:VanillaRunGovernorTerminationCondition\n",
    "    params: {{ }}\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "070883b4",
   "metadata": {},
   "source": [
    "## Runtime Config\n",
    "\n",
    "With the experiment run neatly defined, there is something else that defines how palaestrAI behaves: Its runtime config. It has nothing to do with an experiment run, but defines the behavior of palaestrAI on a certain machine. This includes log levels or the URI defining how to connect to the database. Usually, one does not touch it once the framework is installed.\n",
    "\n",
    "In this case, since we're doing a little tutorial *and* a system test case, we provide some sane defaults that are only relevant for the scope of this notebook. For example, we'll resort to using SQLite in a temporary directory instead of PostgreSQL + TimescaleDB (speed is not of importance here), and we set the log level to `DEBUG` for the store."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2f07fabc",
   "metadata": {},
   "outputs": [],
   "source": [
    "store_dir = tempfile.TemporaryDirectory()\n",
    "store_dir"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9101ad4c",
   "metadata": {},
   "outputs": [],
   "source": [
    "runtime_config = palaestrai.core.RuntimeConfig()\n",
    "runtime_config.reset()\n",
    "runtime_config.load(\n",
    "    {\n",
    "        \"store_uri\": \"sqlite:///%s/palaestrai.db\" % store_dir.name,\n",
    "        \"executor_bus_port\": 4747,\n",
    "        \"logger_port\": 4748,\n",
    "    }\n",
    ")\n",
    "runtime_config.logging[\"loggers\"][\"palaestrai.store\"][\"level\"] = \"DEBUG\"\n",
    "pprint.pprint(runtime_config.to_dict())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de573131",
   "metadata": {},
   "source": [
    "The nice thing about the `RuntimeConfig` is that it is a singleton available everywhere in the framework. So whatever we set here pertains throughout the run."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "07d3398c",
   "metadata": {},
   "source": [
    "## Database Initialization\n",
    "\n",
    "Since we've opted to start fresh with a new SQLite database in a temporary directory, we will have to create and initialize it. Usually, one does this once (e.g., from the CLI with `palaestrai database-create`) and is then done with it, but in this case we do it every time we run the notebook—it is a one-shot tutorial, after all. :-)\n",
    "\n",
    "Luckily, palaestrAI has just the function we need to do it for us:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6d9ad50c",
   "metadata": {},
   "outputs": [],
   "source": [
    "palaestrai.store.database_util.setup_database(runtime_config.store_uri)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90b4dcee",
   "metadata": {},
   "source": [
    "You will see a warning regarding the TimescaleDB extension. That is okay and just a warning. Since we're not running a big, sophisticated experiment, we can live with a bit of a performance penality."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f057b6c4",
   "metadata": {},
   "source": [
    "## Experiment Run Execution\n",
    "\n",
    "Next up: Actually executing the experiment run! It just consists of one line: A call to `palaestrai.execute()`. This method can cope with three types of parameters:\n",
    "\n",
    "1. An `ExperimentRun` object. Nice in cases one has already loaded it (e.g., de-serialized it).\n",
    "2. A `str`. `palaestrAI.execute()` interprets this as a path to a file—one of the most common use cases.\n",
    "3. A `TextIO` object: Any stream that delivers text. Useful when the experiment run document is not yet deserialized, and exactly what we need.\n",
    "\n",
    "To turn a `str` into a `TextIO`, we simply wrap it into a `StringIO` object. Make it so!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f9ad2bc",
   "metadata": {},
   "outputs": [],
   "source": [
    "rc = palaestrai.execute(io.StringIO(experiment_run_document))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb814bcc",
   "metadata": {},
   "source": [
    "The execution should yield no errors (and no warnings, too)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "462254bd",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert rc[1].name == \"EXITED\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a06a9446",
   "metadata": {},
   "source": [
    "## Quering the Store\n",
    "\n",
    "Let's get a custom session to the database first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8fe377d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "dbh = palaestrai.store.Session()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7798b78d",
   "metadata": {},
   "source": [
    "palaestrAI has no special database access features, only nice object-relational mapper (ORM) bindings provided by SQLAlchemy. Which means that we can use all the nice magic SQLAlchemy gives us. So let's first import it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d9c6f1e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sqlalchemy as sa"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d90634a5",
   "metadata": {},
   "source": [
    "Do you remember the name of our experiment run? We can now use it to look it up. Therefore, we first create a query using `sqlalchemy.select`, which we then execute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "283c6f69",
   "metadata": {},
   "outputs": [],
   "source": [
    "q = sa.select(paldb.ExperimentRun).where(\n",
    "    paldb.ExperimentRun.uid == experiment_run_name\n",
    ")\n",
    "str(q)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e78a387c",
   "metadata": {},
   "source": [
    "palaestrAI ensures through the `uid` that each experiment run is stored only once in the database. `one()` not only retrieves only one element from the query, it also raises an exception if there's no or more than one row in the result set. Thus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9947bf58",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = dbh.execute(q).one()\n",
    "experiment_run_record = result[paldb.ExperimentRun]\n",
    "experiment_run_record.id, experiment_run_record.uid"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdf8d8fb",
   "metadata": {},
   "source": [
    "…yes, that's us. \n",
    "\n",
    "No matter how often an experiment run is executed, there will be only one entry for the same UID in the table. But many more instances will exist. Here, since we ran it only once, we will also see only one experiment run instance.\n",
    "\n",
    "Through the SQLAlechemy ORM, we can access the experiment run instances directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93c30ef4",
   "metadata": {},
   "outputs": [],
   "source": [
    "experiment_run_record.experiment_run_instances"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3244ab70",
   "metadata": {},
   "source": [
    "Would we run execute the run again, we'd see two entries in the list here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2966c304",
   "metadata": {},
   "outputs": [],
   "source": [
    "rc = palaestrai.execute(io.StringIO(experiment_run_document))\n",
    "assert rc[1].name == \"EXITED\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "476c1570",
   "metadata": {},
   "outputs": [],
   "source": [
    "dbh.refresh(experiment_run_record)\n",
    "experiment_run_record.experiment_run_instances"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2ad4d0e",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert len(experiment_run_record.experiment_run_instances) > 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc40f611",
   "metadata": {},
   "source": [
    "Now let's focus on the run phases. Each instance will have several of them—three, to be precise. Remember our experiment run document? We have three, so lets find them in the database:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "212026b0",
   "metadata": {},
   "outputs": [],
   "source": [
    "experiment_run_record.experiment_run_instances[0].experiment_run_phases"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9df8d722",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert (\n",
    "    len(\n",
    "        experiment_run_record.experiment_run_instances[0].experiment_run_phases\n",
    "    )\n",
    "    == 3\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b16ed410",
   "metadata": {},
   "source": [
    "Next up: Who did participate in this run phase? We can define participants for each run phase separately. In our experiment run document, we decided that first one agent may train on its own, then we have two agents train together, and finally a test phase for both. So that is what we want to see now.\n",
    "\n",
    "However, simply exploring the ORM is not really fun for showing it in a Jupyter notebook. Thankfully, SQLAlchemy and pandas interface nicely: We can construct a query in SQLAlchemy with our ORM and than end it over to pandas to construct a dataframe out of it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6a61e0bb",
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.read_sql(\n",
    "    sa.select(paldb.Agent).where(\n",
    "        paldb.Agent.experiment_run_phase_id.in_(\n",
    "            phase.id\n",
    "            for phase in experiment_run_record.experiment_run_instances[\n",
    "                0\n",
    "            ].experiment_run_phases\n",
    "        )\n",
    "    ),\n",
    "    dbh.bind,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4a1902e",
   "metadata": {},
   "source": [
    "Okay, now that we have explored many things, let's find out how good our agents were! Let us start by looking at how well the first agent trained when it was alone. Each agent gets a new ID when it enters a new experiment run phase, regardless of whether its the same agent than before or a new one. (The discriminating element is the agent's name.)\n",
    "\n",
    "We first needt the ID of the first experiment run phase:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da9ce13d",
   "metadata": {},
   "outputs": [],
   "source": [
    "run_phase_id = min(\n",
    "    phase.id\n",
    "    for phase in experiment_run_record.experiment_run_instances[\n",
    "        0\n",
    "    ].experiment_run_phases\n",
    ")\n",
    "run_phase_id"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f255bd8f",
   "metadata": {},
   "source": [
    "Okay, which agent is it?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "71377cbb",
   "metadata": {},
   "outputs": [],
   "source": [
    "agent_record = dbh.execute(\n",
    "    sa.select(paldb.Agent).where(\n",
    "        paldb.Agent.experiment_run_phase_id == run_phase_id\n",
    "    )\n",
    ").one()[paldb.Agent]\n",
    "assert agent_record.name == \"mighty_defender\"\n",
    "agent_record.id, agent_record.name"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2495ce6d",
   "metadata": {},
   "outputs": [],
   "source": [
    "actions = pd.read_sql(\n",
    "    sa.select(paldb.MuscleAction).where(\n",
    "        paldb.MuscleAction.agent_id == agent_record.id\n",
    "    ),\n",
    "    dbh.bind,\n",
    ")\n",
    "actions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55f697e0",
   "metadata": {},
   "source": [
    "Okay, but how do we get rewards out of this? The `rewards` column contains a list of `RewardInformation` objects. In our case, we know that there will ever be only one (more than one is a special case). We also know that there will always be a float. The knowledge about this comes from our knowledge of the reward, i.e., it is really domain knowledge that an experimenter will have.\n",
    "\n",
    "At this point, we need to modify the dataframe a bit. We have to call `jsonpickle.loads()` to get the object, and then extract the reward out of it. `DataFrame.apply()` solves us well here. In order to make it more readable, we provide a function for this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "321c68bd",
   "metadata": {},
   "outputs": [],
   "source": [
    "def unpack_reward(x):\n",
    "    return float(x[0][\"reward_value\"]) if x else 0.0\n",
    "\n",
    "\n",
    "actions.rewards = actions.rewards.apply(lambda x: unpack_reward(x))\n",
    "actions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1938151e",
   "metadata": {},
   "source": [
    "Plotting is relatively easy now, as pandas already provides us with everything we need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9df35acd",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "actions.plot(x=\"id\", y=\"rewards\", kind=\"scatter\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "849a5726",
   "metadata": {},
   "source": [
    "Okay, but if we want to compare the agents' performance during the testing phase? First we need to find out what agents participated in the last experiment run phase. So let's return to the experiment run phases table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d10eb910",
   "metadata": {},
   "outputs": [],
   "source": [
    "experiment_run_phases = pd.read_sql(\n",
    "    sa.select(paldb.Agent)\n",
    "    .where(\n",
    "        paldb.Agent.experiment_run_phase_id.in_(\n",
    "            phase.id\n",
    "            for phase in experiment_run_record.experiment_run_instances[\n",
    "                0\n",
    "            ].experiment_run_phases\n",
    "        )\n",
    "    )\n",
    "    .order_by(paldb.Agent.experiment_run_phase_id.desc()),\n",
    "    dbh.bind,\n",
    ")\n",
    "experiment_run_phases"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "642b18b5",
   "metadata": {},
   "source": [
    "Okay, the top two rows are the ones we want to look at."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c3786fba",
   "metadata": {},
   "outputs": [],
   "source": [
    "muscle_actions = pd.read_sql(\n",
    "    sa.select(paldb.Agent, paldb.MuscleAction)\n",
    "    .join(paldb.Agent.muscle_actions)\n",
    "    .where(\n",
    "        paldb.Agent.experiment_run_phase_id.in_(\n",
    "            experiment_run_phases.experiment_run_phase_id[0:2]\n",
    "        )\n",
    "    ),\n",
    "    dbh.bind,\n",
    ")\n",
    "assert len(muscle_actions) > 2\n",
    "muscle_actions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56316683",
   "metadata": {},
   "source": [
    "Let's do the reward conversion dance:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b4947c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "muscle_actions.rewards = muscle_actions.rewards.apply(\n",
    "    lambda x: unpack_reward(x)\n",
    ")\n",
    "muscle_actions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b8c62e2",
   "metadata": {},
   "source": [
    "The table contains rewards, alternating, for both agents. You can see that from the `simtime_ticks` entry as well the `name` column. So let's plot them—it's easy now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad47cbee",
   "metadata": {},
   "outputs": [],
   "source": [
    "defender_actions = muscle_actions[muscle_actions.name == \"mighty_defender\"][\n",
    "    [\"rewards\"]\n",
    "].rename(columns={\"rewards\": \"defender_rewards\"})\n",
    "attacker_actions = muscle_actions[muscle_actions.name == \"evil_attacker\"][\n",
    "    [\"rewards\"]\n",
    "].rename(columns={\"rewards\": \"attacker_rewards\"})\n",
    "pd.concat([attacker_actions, defender_actions]).plot()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
