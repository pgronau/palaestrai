*** Settings ***
Documentation   Integration Test of DoE package arsenAI
...
...             PalaestrAI has a sister package 'arsenAI' which
...             which can generate a number of experiment files as result
...             of a design of experiments (DoE).
...             This test is designed to test the creation and
...             validity of these files.

Library         Process
Library         OperatingSystem
Library         yaml
Library         Collections
Suite Teardown  Clean Files
Suite Setup     Pull arsenAI

*** Variables ***
${ARSENAI_SRC_DIR}                  ${TEMPDIR}${/}arsenai
${OUTPUTS_DIR}                      ${CURDIR}${/}_outputs

*** Keywords ***
Clean Files
    Remove File                     ${TEMPDIR}${/}arsenai_std???.txt
    Remove File                     ${TEMPDIR}${/}palaestrai_???.txt
    Remove Directory                ${ARSENAI_SRC_DIR}  recursive=true
    Remove Directory                ${OUTPUTS_DIR}  recursive=true

Pull arsenAI
    Directory Should Not Exist    ${ARSENAI_SRC_DIR}
    ${result} =                     Run Process  git  clone  -b  development  https://gitlab.com/arl2/arsenai.git  ${ARSENAI_SRC_DIR}
    Log Many                        ${result.stdout}  ${result.stderr}
    Directory Should Exist         ${ARSENAI_SRC_DIR}${/}tests${/}fixtures
    Create Directory                ${OUTPUTS_DIR}

*** Test Cases ***
Test example experiment file
    [Timeout]                       60
    [Teardown]                      Clean Files
    Copy File                       ${ARSENAI_SRC_DIR}${/}tests${/}fixtures${/}example_experiment.yml  ${OUTPUTS_DIR}
    Log File                        ${OUTPUTS_DIR}${/}example_experiment.yml
    ${result_arsenai} =             Run Process  arsenai  generate  ${OUTPUTS_DIR}${/}example_experiment.yml  stdout=${TEMPDIR}${/}arsenai_stdout.txt  stderr=${TEMPDIR}${/}arsenai_stderr.txt  cwd=${OUTPUTS_DIR}
    Log Many                        ${result_arsenai.stdout}  ${result_arsenai.stderr}
    Should Be Equal As Integers     ${result_arsenai.rc}  0
    @{paths} =                      List Files In Directory  ${OUTPUTS_DIR}${/}_outputs  pattern=*.yml  absolute=true
    FOR  ${path}  IN  @{paths}
        Log File                    ${path}
        ${result_palaestrai} =      Run Process  palaestrai   experiment-check-syntax  ${path}  stdout=${TEMPDIR}${/}palaestrai_stdout.txt  stderr=${TEMPDIR}${/}palaestrai_stderr.txt
        Log Many                    ${result_palaestrai.stdout}  ${result_palaestrai.stderr}
        Should Be Equal As Integers  ${result_palaestrai.rc}  0
    END

