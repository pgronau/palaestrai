from unittest import TestCase

import numpy as np
import pandas as pd
from pandas import testing as pdt

from palaestrai.agent import (
    RewardInformation,
    ActuatorInformation,
    SensorInformation,
)
from palaestrai.agent.memory import Memory
from palaestrai.types import Box


class TestMemory(TestCase):
    def setUp(self) -> None:
        self.rewards = [
            RewardInformation(
                1,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "reward_1",
            ),
            RewardInformation(
                2,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "reward_2",
            ),
        ]
        self.rewards2 = [
            RewardInformation(
                3,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "reward_1",
            ),
            RewardInformation(
                4,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "reward_2",
            ),
        ]
        self.actions = [
            ActuatorInformation(
                1,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "action_1",
            ),
            ActuatorInformation(
                2,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "action_2",
            ),
        ]
        self.actions2 = [
            ActuatorInformation(
                3,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "action_1",
            ),
            ActuatorInformation(
                4,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "action_2",
            ),
        ]
        self.observations = [
            SensorInformation(
                1,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "observation_1",
            ),
            SensorInformation(
                2,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "observation_2",
            ),
        ]
        self.observations2 = [
            SensorInformation(
                3,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "observation_1",
            ),
            SensorInformation(
                4,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "observation_2",
            ),
        ]
        self.internal_rewards = 1.0
        self.internal_rewards2 = 2.0
        self.memory = Memory(self.rewards, self.actions, self.observations)

    def test_add_with_correct_information_to_empty_memory(self):
        self.memory.append(
            self.rewards,
            self.actions,
            self.observations,
            self.internal_rewards,
        )
        pdt.assert_series_equal(
            self.memory.rewards.iloc[-1],
            pd.Series([1.0, 2.0]),
            check_index=False,
            check_names=False,
        )
        pdt.assert_series_equal(
            self.memory.actions.iloc[-1],
            pd.Series([1.0, 2.0]),
            check_index=False,
            check_names=False,
        )
        pdt.assert_series_equal(
            self.memory.observations.iloc[-1],
            pd.Series([1.0, 2.0]),
            check_index=False,
            check_names=False,
        )
        pdt.assert_series_equal(
            self.memory.internal_reward.iloc[-1],
            pd.Series([1.0]),
            check_index=False,
            check_names=False,
        )

    def test_add_with_correct_information_to_memory(self):
        self.memory.append(
            self.rewards,
            self.actions,
            self.observations,
            self.internal_rewards,
        )
        self.memory.append(
            self.rewards2,
            self.actions2,
            self.observations2,
            self.internal_rewards2,
        )
        pdt.assert_series_equal(
            self.memory.rewards.iloc[-1],
            pd.Series([3.0, 4.0]),
            check_index=False,
            check_names=False,
        )
        pdt.assert_series_equal(
            self.memory.actions.iloc[-1],
            pd.Series([3.0, 4.0]),
            check_index=False,
            check_names=False,
        )
        pdt.assert_series_equal(
            self.memory.observations.iloc[-1],
            pd.Series([3.0, 4.0]),
            check_index=False,
            check_names=False,
        )
        pdt.assert_series_equal(
            self.memory.internal_reward.iloc[-1],
            pd.Series([2.0]),
            check_index=False,
            check_names=False,
        )

    def test_add_with_incorrect_env_rewards(self):
        env_rewards = "Test"
        with self.assertRaises(AttributeError):
            self.memory.append(
                env_rewards,
                self.actions,
                self.observations,
                self.internal_rewards,
            )
        env_rewards = {"Test": "Test"}
        with self.assertRaises(AttributeError):
            self.memory.append(
                env_rewards,
                self.actions,
                self.observations,
                self.internal_rewards,
            )
        env_rewards = [1, 2, 3]
        with self.assertRaises(AttributeError):
            self.memory.append(
                env_rewards,
                self.actions,
                self.observations,
                self.internal_rewards,
            )
        env_rewards = [
            SensorInformation(
                1,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "sensor_1",
            )
        ]
        with self.assertRaises(AttributeError):
            self.memory.append(
                env_rewards,
                self.actions,
                self.observations,
                self.internal_rewards,
            )
        env_rewards = [
            ActuatorInformation(
                1,
                Box(low=0, high=10, shape=(1,), dtype=np.float32),
                "actuator_1",
            )
        ]
        with self.assertRaises(AttributeError):
            self.memory.append(
                env_rewards,
                self.actions,
                self.observations,
                self.internal_rewards,
            )

    def test_add_env_rewards(self):
        self.memory.append_rewards(self.rewards)
        pdt.assert_series_equal(
            self.memory.rewards.iloc[-1],
            pd.Series([1.0, 2.0]),
            check_index=False,
            check_names=False,
        )

    def test_add_actions(self):
        self.memory.append_actions(self.actions)
        pdt.assert_series_equal(
            self.memory.actions.iloc[-1],
            pd.Series([1.0, 2.0]),
            check_index=False,
            check_names=False,
        )

    def test_add_observations(self):
        self.memory.append_observations(self.observations)
        pdt.assert_series_equal(
            self.memory.observations.iloc[-1],
            pd.Series([1.0, 2.0]),
            check_index=False,
            check_names=False,
        )

    def test_add_rewards(self):
        self.memory.append_internal_rewards(self.internal_rewards)
        pdt.assert_series_equal(
            self.memory.internal_reward.iloc[-1],
            pd.Series([1.0]),
            check_index=False,
            check_names=False,
        )

    def test_get_last_row(self):
        self.memory.append(
            self.rewards,
            self.actions,
            self.observations,
            self.internal_rewards,
        )
        self.memory.append(
            self.rewards2,
            self.actions2,
            self.observations2,
            self.internal_rewards2,
        )
        df1 = pd.DataFrame(
            [[3.0, 4.0]],
            columns=["reward_1", "reward_2"],
        )
        df2 = pd.DataFrame(
            [[3.0, 4.0]],
            columns=["action_1", "action_2"],
        )
        df3 = pd.DataFrame(
            [[3.0, 4.0]],
            columns=["observation_1", "observation_2"],
        )
        df4 = pd.DataFrame(
            [[2.0]],
            columns=["internal_reward"],
        )
        t = (df1, df2, df3, df4)
        t = (df1, df2, df3, df4)
        for x, y in zip(self.memory.get_last_rows(1), t):
            pdt.assert_frame_equal(
                x.reset_index(drop=True), y.reset_index(drop=True)
            )

    def test_get_last_rows(self):
        for i in range(5):
            self.memory.append(
                self.rewards,
                self.actions,
                self.observations,
                self.internal_rewards,
            )
            self.memory.append(
                self.rewards2,
                self.actions2,
                self.observations2,
                self.internal_rewards2,
            )
        df1 = pd.DataFrame(
            [[3.0, 4.0], [1.0, 2.0], [3.0, 4.0]],
            columns=["reward_1", "reward_2"],
        )
        df2 = pd.DataFrame(
            [[3.0, 4.0], [1.0, 2.0], [3.0, 4.0]],
            columns=["action_1", "action_2"],
        )
        df3 = pd.DataFrame(
            [[3.0, 4.0], [1.0, 2.0], [3.0, 4.0]],
            columns=["observation_1", "observation_2"],
        )
        df4 = pd.DataFrame(
            [[2.0], [1.0], [2.0]],
            columns=["internal_reward"],
        )
        t = (df1, df2, df3, df4)
        for x, y in zip(self.memory.get_last_rows(3), t):
            pdt.assert_frame_equal(
                x.reset_index(drop=True), y.reset_index(drop=True)
            )

    def test_get_last_reward(self):
        self.memory.append_rewards(self.rewards)
        pdt.assert_series_equal(
            self.memory.get_last_reward(),
            pd.Series([1.0, 2.0]),
            check_index=False,
            check_names=False,
        )

    def test_get_last_action(self):
        self.memory.append_actions(self.actions)
        pdt.assert_series_equal(
            self.memory.get_last_action(),
            pd.Series([1.0, 2.0]),
            check_index=False,
            check_names=False,
        )

    def test_get_last_observation(self):
        self.memory.append_observations(self.observations)
        pdt.assert_series_equal(
            self.memory.get_last_observation(),
            pd.Series([1.0, 2.0]),
            check_index=False,
            check_names=False,
        )

    def test_get_last_internal_reward(self):
        self.memory.append_internal_rewards(self.internal_rewards)
        pdt.assert_series_equal(
            self.memory.get_last_internal_reward(),
            pd.Series([1.0]),
            check_index=False,
            check_names=False,
        )

    def test_get_full_memory(self):
        self.memory.append(
            self.rewards2,
            self.actions2,
            self.observations2,
            self.internal_rewards2,
        )
        self.memory.append(
            self.rewards,
            self.actions,
            self.observations,
            self.internal_rewards,
        )
        self.memory.append(
            self.rewards2,
            self.actions2,
            self.observations2,
            self.internal_rewards2,
        )
        result = self.memory.get_full_memory()
        df1 = pd.DataFrame(
            [[3.0, 4.0], [1.0, 2.0], [3.0, 4.0]],
            columns=["reward_1", "reward_2"],
        )
        df2 = pd.DataFrame(
            [[3.0, 4.0], [1.0, 2.0], [3.0, 4.0]],
            columns=["action_1", "action_2"],
        )
        df3 = pd.DataFrame(
            [[3.0, 4.0], [1.0, 2.0], [3.0, 4.0]],
            columns=["observation_1", "observation_2"],
        )
        df4 = pd.DataFrame(
            [[2.0], [1.0], [2.0]],
            columns=["internal_reward"],
        )
        t = (df1, df2, df3, df4)
        for x, y in zip(self.memory.get_full_memory(), t):
            pdt.assert_frame_equal(
                x.reset_index(drop=True), y.reset_index(drop=True)
            )
