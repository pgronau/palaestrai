palaestrAI Architecture
-----------------------

palaestrAI has a distributed architecture at its core. The following
documents help you to understand the internals of palaestrAI itself. This
is useful if you want to contribute to palaestrAI itself.

.. note::
    The following documents are not relevant for you if you are a user of
    palaestrAI (the core framework itself). This applies if you want to
    contribute an agent implementation, algorithm, environment, objectives,
    etc.

    If you are interested in contributing new agent implementations, please
    have a look at the `Brain-Muscle API <brain-muscle-api.rst>`_.

    Creating new environments is documented in the corresponding `section
    on environments <environments.rst>`_

.. toctree::
    :numbered:

    Sequence diagram: A sequence diagram showing an experiment run <./architecture/00-sequence_diagram>
    Run governor diagram: A diagram showing the different states of the :class:`~RunGovernor` <./architecture/01-run_governor_diagram>