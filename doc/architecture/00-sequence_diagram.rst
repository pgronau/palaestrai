Sequence Diagram of an experiment run
=====================================

.. image:: ../_static/experiment_sequence_diagram.svg

Each experiment run can be split up in three parts:

1. The initialization
2. The main loop
3. Shutdown 


Initialization
--------------

After starting an experiment run with the CLI using the command ``palaestrai
experiment-start path/to/experiment``, the initialization process starts by
handing the experiment path over to the :class:`~Executor`'s
:meth:`~Executor.execute` method. The :class:`~Executor` first launches the
:class:`~RunGovernor` and sends an :class:`~ExperimentStartRequest` to it
afterwards. The :class:`~RunGovernor` handles most of the initialization:

1. It sets up the :class:`~SimulationController`, 
   :class:`~EnviornmentConductor`, and :class:`AgentConductor` (in order)
2. Sends a :class:`~SimulationStartRequest` to the 
   :class:`~SimulationController`, which proceeds to request the 
   initialization of the :class:`~Enviornment` from the 
   :class:`~EnvironmentConductor`, and starts it afterwards.
3. Then the SimulationController sends a :class:`~AgentSetupRequest` 
   to the :class:`~AgentConductor`, which now initializes all needed 
   brains and muscles for the experiment.

After receiving an :class:`~AgentSetupResponse` from the
:class:`~AgentConductor`, the :class:`~RunGovernor` changes back into the
``Transceiving`` state and waits for new Requests. This concludes the
initialization part of the experiment run.

Main Loop
---------

The Main Loop is mainly a back and forth between the
:class:`~SimulationController` and the :class:`~Environment` or
:class:`~Agent`s. The SimulationController sends either an
:class:`~EnvironmentUpdateRequest` to the Environment, which then updates and
responds with an :class:`~EnvironmentUpdateResponse`, or an
:class:`~AgentUpdateRequest` to a muscle, which then sends a
:class:`~MuscleUpdateRequest` to its corresponding brain. After receiving a
:class:`~MuscleUpdateResponse` from its brain, the muscle can finally respond
to the :class:`~SimulationController` with an :class:`~AgentUpdateResponse`.


Shutdown
--------

The shutdown usually gets initialized by an :class:`~EnvironmentUpdateResponse`
with the ``is_terminal=True`` flag. The Executor then sends an 
``ExpermimentRunShutdownRequest`` to the RunGovernor. The RunGovernor forwards
this request in form of a ``SimulationStopRequest`` to the
SimulationController which in turn will send shutdown requests to all muscles,
which forward those to their brains, Environments and finally Agent- and
EnvironmentConductors. After receiving a shutdown response from everyone the
SimulationController sends a ``SimulationStopResponse`` to the RunGovernor and
gets terminated. The RunGovernor forwards the Response as
``ExperimentShutdownResponse`` to the Executor then cleans up any left over
sub processes and shuts down aswell.
