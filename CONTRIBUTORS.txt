The first prototype of ARL was developed by Jan-Menno Memmen and
Lars Fischer.

Contributors (by order of appearance) are:

Lars Fischer <lars.fischer@hs-bremerhaven.de>
Jan-Menno Memmen <janmenno@yahoo.de>
Eric MSP Veith <eric.veith@offis.de>
Lasse Hammer <lasse.hammer@offis.de>
Stephan Balduin <stephan.balduin@offis.de>
David Winkler <david.winkler@uol.de>
Paul Albrecht Gronau <paul.gronau@offis.de>
Maria Edith Elizondo Guerrero <maria.edith.elizondo.guerrero@uni-oldenburg.de>
Nils Wenninghoff <nils.wenninghoff@offis.de>
Sebastian Wiele <sebastian.wiele@technik-emden.de>
Marcel Baumann <marcel.baumann@uni-oldenburg.de>
Sharaf Aldin Alsharif <sharaf.aldin.alsharif@uni-oldenburg.de>
Daniel Lange <daniel.lange@offis.de>
Erika Puiutta <erica.puiutta@offis.de>
Thomas Wolgast <thomas.wolgast@offis.de>
